package com.br.rba.concrete;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.br.rba.concrete.adapter.RepositoriesAdapter;
import com.br.rba.concrete.commons.Constants;
import com.br.rba.concrete.listener.EndlessRecyclerViewScrollListener;
import com.br.rba.concrete.listener.RecyclerItemClickListener;
import com.br.rba.concrete.listener.SnackbarListener;
import com.br.rba.concrete.model.Repository;
import com.br.rba.concrete.service.Service;
import com.br.rba.concrete.service.ServiceRequestListener;
import com.br.rba.concrete.utils.ConnectionUtils;
import com.br.rba.concrete.utils.SnackbarUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RepositoryListActivity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private RecyclerView repositoriesRecycleView;
    private ProgressBar progressBar;
    private RepositoriesAdapter mAdapter;
    private ArrayList<Repository> repositoriesList = new ArrayList<>();
    private Gson gson;
    private EndlessRecyclerViewScrollListener scrollListener;
    private LinearLayoutManager linearLayoutManager;
    private Parcelable listState;
    private LinearLayout linearProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository_list);
        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        relativeLayout = findViewById(R.id.relative_layout_repositories);
        progressBar = findViewById(R.id.progressbar);
        linearProgress = findViewById(R.id.linear_progress);
        repositoriesRecycleView = findViewById(R.id.recycler_view_repositories);
        initGson();
        setRecycleView();
        if (savedInstanceState != null) {
            restoreRecycleViewState();
        } else {
            linearProgress.setVisibility(View.VISIBLE);
            loadRepositories(1);
        }
    }

    private void restoreRecycleViewState() {
        repositoriesList = getArrayList(Constants.REPOSITORIES_LIST);
        scrollListener.setCurrentPage(getPage(Constants.CURRENT_PAGE));
        setRecycleViewAdapter();
        dismissProgress();
    }

    private void initGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    private void setRecycleView() {
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        repositoriesRecycleView.setLayoutManager(linearLayoutManager);
        repositoriesRecycleView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                callPullRequestsListActivity(repositoriesList.get(position));
            }
        }));
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                progressBar.setVisibility(View.VISIBLE);
                loadRepositories(page);
            }
        };
        repositoriesRecycleView.addOnScrollListener(scrollListener);
    }

    private void callPullRequestsListActivity(Repository repo) {
        Intent it = new Intent(this, PullRequestsListActivity.class);
        it.putExtra(Constants.REPOSITORY, repo);
        startActivity(it);
    }

    private void loadRepositories(final int page) {
        if (ConnectionUtils.isOnline(this)) {
            Service.getRepositories(page, new ServiceRequestListener() {
                @Override
                public void onResponseObject(JSONObject response) {
                    if (response != null) {
                        JSONArray jsonArray = response.optJSONArray(Constants.TAG_ITEMS);
                        if (jsonArray != null) {
                            List<Repository> moreRepositories = Arrays.asList(gson.fromJson(jsonArray.toString(), Repository[].class));
                            repositoriesList.addAll(moreRepositories);
                            setRecycleViewAdapter();
                        }
                    }
                    dismissProgress();
                }

                @Override
                public void onResponseArray(JSONArray response) {

                }

                @Override
                public void onError(String error) {
                    dismissProgress();
                    SnackbarUtils.showSnackBar(relativeLayout, error);
                }
            });
        } else {
            dismissProgress();
            SnackbarUtils.showNoConnectionSnackBar(relativeLayout, getResources().getString(R.string.no_connection),
                    new SnackbarListener() {
                        @Override
                        public void onResponse(boolean retry) {
                            if (retry) {
                                loadRepositories(page);
                            }
                        }
                    });
        }
    }

    private void setRecycleViewAdapter() {
        if (mAdapter == null) {
            mAdapter = new RepositoriesAdapter(getApplicationContext(), repositoriesList, repositoriesRecycleView);
            repositoriesRecycleView.setAdapter(mAdapter);
        } else {
            final int curSize = mAdapter.getItemCount();
            repositoriesRecycleView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyItemRangeInserted(curSize,
                            repositoriesList.size() - 1);
                    dismissProgress();
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        listState = linearLayoutManager.onSaveInstanceState();
        state.putParcelable(Constants.LIST_STATE, listState);
        saveArrayListAndPage(repositoriesList, Constants.REPOSITORIES_LIST,
                scrollListener.getCurrentPage(), Constants.CURRENT_PAGE);
    }

    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state != null) {
            listState = state.getParcelable(Constants.LIST_STATE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (listState != null) {
            linearLayoutManager.onRestoreInstanceState(listState);
        }
    }

    private void dismissProgress() {
        if(progressBar.getVisibility() == View.VISIBLE){
            progressBar.setVisibility(View.GONE);
        }

        if(linearProgress.getVisibility() == View.VISIBLE){
            linearProgress.setVisibility(View.GONE);
        }
    }

    public void saveArrayListAndPage(ArrayList<Repository> list, String key, int page, String pageKey) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.putInt(pageKey, page);
        editor.apply();
    }

    public ArrayList<Repository> getArrayList(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<Repository>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public int getPage(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getInt(key, 1);
    }
}