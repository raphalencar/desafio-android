package com.br.rba.concrete.service;

import android.app.Application;

import com.br.rba.concrete.commons.Constants;

public class ServiceApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Service.getInstance().init(this);
    }

    public static String getRepositoriesUrl(int page) {
        return Constants.REPOSITORIES_URL + page;
    }

    public static String getPullRequestsUrl(String owner, String repo) {
        StringBuilder builder = new StringBuilder();
        builder.append(owner);
        builder.append("/");
        builder.append(repo);
        return Constants.PULL_REQUESTS_URL + builder + Constants.PULLS;
    }
}