package com.br.rba.concrete.service;

import android.app.Application;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class Service {
    private static RequestQueue mRequestQueue;
    private static Service mInstance;
    private static Context mApplicationContext;

    private Service() {
        super();
    }

    public static Service getInstance() {
        if (mInstance == null) {
            mInstance = new Service();
        }
        return mInstance;
    }

    public void init(Application application) {
        if (mApplicationContext == null) {
            mApplicationContext = application.getApplicationContext();
            mRequestQueue = Volley.newRequestQueue(mApplicationContext);
        }
    }

    public static void getRepositories(int page, final ServiceRequestListener listener) {
        JsonObjectRequest request = new JsonObjectRequest
                (Request.Method.GET,
                        getRepositoriesUrl(page),
                        null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                listener.onResponseObject(response);
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        mRequestQueue.add(request);
    }


    public static void getPullRequests(String owner, String repo, final ServiceRequestListener listener) {
        JsonArrayRequest request = new JsonArrayRequest
                (Request.Method.GET,
                        getPullRequestsUrl(owner, repo),
                        null,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                listener.onResponseArray(response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        mRequestQueue.add(request);
    }

    private static String getPullRequestsUrl(String owner, String repo) {
        return ServiceApplication.getPullRequestsUrl(owner, repo);
    }

    private static String getRepositoriesUrl(int page) {
        return ServiceApplication.getRepositoriesUrl(page);
    }
}
