package com.br.rba.concrete.commons;

public class Constants {
    public static final String RETRY = "Tentar Novamente";
    public static final String REPOSITORIES_URL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";
    public static final String PULL_REQUESTS_URL = "https://api.github.com/repos/";
    public static final String PULLS = "/pulls";
    public static final String TAG_ITEMS = "items";
    public static final String REPOSITORY = "repository";
    public static final String REPOSITORIES_LIST = "repositories_list";
    public static final String PULL_REQUESTS_LIST = "pull_request_list";
    public static final String LIST_STATE = "list_state";
    public static final String CURRENT_PAGE = "page";
}
