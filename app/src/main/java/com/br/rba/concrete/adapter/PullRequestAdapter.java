package com.br.rba.concrete.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.rba.concrete.R;
import com.br.rba.concrete.model.PullRequest;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestsViewHolder> {

    private Context mContext;
    private List<PullRequest> pullRequestList;

    public PullRequestAdapter(Context mContext, List<PullRequest> pullRequestList) {
        this.mContext = mContext;
        this.pullRequestList = pullRequestList;
    }

    public class PullRequestsViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView date;
        private TextView body;
        private ImageView avatarUrl;
        private TextView login;

        private PullRequestsViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.pull_request_title);
            date = itemView.findViewById(R.id.pull_request_date);
            body = itemView.findViewById(R.id.pull_request_body);
            avatarUrl = itemView.findViewById(R.id.pull_request_avatar);
            login = itemView.findViewById(R.id.pull_request_username);
        }
    }

    @Override
    public PullRequestAdapter.PullRequestsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pull_request_card, parent, false);
        return new PullRequestAdapter.PullRequestsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PullRequestAdapter.PullRequestsViewHolder holder, int position) {
        PullRequest pr = pullRequestList.get(position);
        if (pr != null) {
            holder.title.setText(pr.getTitle());
            holder.date.setText(getDateFormat(pr.getDate()));
            holder.body.setText(pr.getBody());
            Glide.with(mContext).load(pr.getUser().getAvatarUrl()).into(holder.avatarUrl);
            holder.login.setText(pr.getUser().getLogin());
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String getDateFormat(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    @Override
    public int getItemCount() {
        return pullRequestList.size();
    }
}
