package com.br.rba.concrete;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.br.rba.concrete.adapter.PullRequestAdapter;
import com.br.rba.concrete.commons.Constants;
import com.br.rba.concrete.listener.RecyclerItemClickListener;
import com.br.rba.concrete.listener.SnackbarListener;
import com.br.rba.concrete.model.PullRequest;
import com.br.rba.concrete.model.Repository;
import com.br.rba.concrete.service.Service;
import com.br.rba.concrete.service.ServiceRequestListener;
import com.br.rba.concrete.utils.ConnectionUtils;
import com.br.rba.concrete.utils.SnackbarUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PullRequestsListActivity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private RecyclerView pullRequestsRecycleView;
    private ArrayList<PullRequest> pullRequestList;
    private PullRequestAdapter mAdapter;
    private Gson gson;
    private Repository repo;
    private LinearLayoutManager linearLayoutManager;
    private Parcelable listState;
    private LinearLayout linearProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests_list);
        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        getRepository();
        relativeLayout = findViewById(R.id.relative_layout_pull_request);
        linearProgress = findViewById(R.id.linear_progress_pull);
        pullRequestsRecycleView = findViewById(R.id.recycler_view_pull_requests);
        pullRequestList = new ArrayList<>();
        initGson();
        setRecycleView();
        if (savedInstanceState != null) {
            pullRequestList = getArrayList(Constants.PULL_REQUESTS_LIST);
            setRecycleViewAdapter();
            linearProgress.setVisibility(View.GONE);
        } else {
            loadPullRequests();
        }
    }

    private void getRepository() {
        if (repo == null) {
            repo = new Repository();
        }
        if (getIntent().hasExtra(Constants.REPOSITORY)) {
            repo = getIntent().getParcelableExtra(Constants.REPOSITORY);
        }
    }

    private void initGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    private void setRecycleView() {
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        pullRequestsRecycleView.setLayoutManager(linearLayoutManager);
        pullRequestsRecycleView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                callUrl(position);
            }
        }));
    }

    private void callUrl(int position) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(pullRequestList.get(position).getUrl()));
        startActivity(i);
    }

    private void loadPullRequests() {
        linearProgress.setVisibility(View.VISIBLE);
        if (ConnectionUtils.isOnline(this)) {
            Service.getPullRequests(repo.getOwner().getLogin(),
                    repo.getName(), new ServiceRequestListener() {
                        @Override
                        public void onResponseObject(JSONObject response) {
                        }

                        @Override
                        public void onResponseArray(JSONArray response) {
                            if (response != null) {
                                Type listType = new TypeToken<List<PullRequest>>() {
                                }.getType();
                                List<PullRequest> pullRequests = gson.fromJson(response.toString(), listType);
                                pullRequestList.addAll(pullRequests);
                                setRecycleViewAdapter();
                            }
                            linearProgress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(String error) {
                            linearProgress.setVisibility(View.GONE);
                            SnackbarUtils.showSnackBar(relativeLayout, error);
                        }
                    });
        } else {
            linearProgress.setVisibility(View.GONE);
            SnackbarUtils.showNoConnectionSnackBar(relativeLayout, getResources().getString(R.string.no_connection),
                    new SnackbarListener() {
                        @Override
                        public void onResponse(boolean retry) {
                            if (retry) {
                                loadPullRequests();
                            }
                        }
                    });
        }
    }

    private void setRecycleViewAdapter() {
        if (mAdapter == null) {
            mAdapter = new PullRequestAdapter(getApplicationContext(), pullRequestList);
            pullRequestsRecycleView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        listState = linearLayoutManager.onSaveInstanceState();
        state.putParcelable(Constants.LIST_STATE, listState);
        saveArrayList(pullRequestList, Constants.PULL_REQUESTS_LIST);
    }

    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state != null) {
            listState = state.getParcelable(Constants.LIST_STATE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (listState != null) {
            linearLayoutManager.onRestoreInstanceState(listState);
        }
    }

    public void saveArrayList(ArrayList<PullRequest> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();
    }

    public ArrayList<PullRequest> getArrayList(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<PullRequest>>() {
        }.getType();
        return gson.fromJson(json, type);
    }
}
