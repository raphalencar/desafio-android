package com.br.rba.concrete.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Repository implements Parcelable {
    private String id;
    private String name;
    private String description;
    @SerializedName("forks_count")
    private int forks;
    @SerializedName("stargazers_count")
    private int stars;
    private Owner owner;

    public Repository() {
    }

    public Repository(String id, String name, String description, int forks, int stars, Owner owner) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.forks = forks;
        this.stars = stars;
        this.owner = owner;
    }

    protected Repository(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        forks = in.readInt();
        stars = in.readInt();
        owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(forks);
        parcel.writeInt(stars);
        parcel.writeParcelable(owner, i);
    }
}
