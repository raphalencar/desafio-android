package com.br.rba.concrete.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class PullRequest implements Parcelable {
    private String id;
    private String title;
    private String body;
    @SerializedName("created_at")
    private Date date;
    @SerializedName("html_url")
    private String url;
    private User user;

    public PullRequest(String id, String title, String body, Date date, String url, User user) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.date = date;
        this.url = url;
        this.user = user;
    }

    protected PullRequest(Parcel in) {
        id = in.readString();
        title = in.readString();
        body = in.readString();
        url = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel in) {
            return new PullRequest(in);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(url);
        parcel.writeParcelable(user, i);
    }
}
