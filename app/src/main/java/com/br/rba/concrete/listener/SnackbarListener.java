package com.br.rba.concrete.listener;

public interface SnackbarListener {
    void onResponse(boolean retry);
}

