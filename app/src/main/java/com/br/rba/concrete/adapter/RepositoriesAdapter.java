package com.br.rba.concrete.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.rba.concrete.R;
import com.br.rba.concrete.model.Repository;
import com.bumptech.glide.Glide;

import java.util.List;

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder> {

    private Context mContext;
    private List<Repository> repositoriesList;

    public RepositoriesAdapter(Context mContext, List<Repository> repositoriesList, RecyclerView recyclerView) {
        this.mContext = mContext;
        this.repositoriesList = repositoriesList;
    }

    @Override
    public int getItemViewType(int position) {
        return repositoriesList.get(position) != null ? 1 : 0;
    }

    public List<Repository> getList() {
        return repositoriesList;
    }

    public class RepositoriesViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView description;
        private ImageView avatarUrl;
        private TextView login;
        private TextView forks;
        private TextView stars;

        private RepositoriesViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.repository_name);
            description = itemView.findViewById(R.id.repository_description);
            avatarUrl = itemView.findViewById(R.id.repository_profile);
            login = itemView.findViewById(R.id.repository_username);
            forks = itemView.findViewById(R.id.repository_forks);
            stars = itemView.findViewById(R.id.repository_stars);
        }
    }

    @Override
    public RepositoriesAdapter.RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repository_card, parent, false);
        return new RepositoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RepositoriesAdapter.RepositoriesViewHolder holder, int position) {
        Repository repo = repositoriesList.get(position);
        if (repo != null) {
            holder.name.setText(repo.getName());
            holder.description.setText(repo.getDescription());
            if(repo.getOwner()!=null){
                Glide.with(mContext).load(repo.getOwner().getAvatarUrl()).into(holder.avatarUrl);
                holder.login.setText(repo.getOwner().getLogin());
            }
            holder.forks.setText(String.valueOf(repo.getForks()));
            holder.stars.setText(String.valueOf(repo.getStars()));
        }
    }

    @Override
    public int getItemCount() {
        return repositoriesList.size();
    }
}
