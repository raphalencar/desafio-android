package com.br.rba.concrete.service;

import org.json.JSONArray;
import org.json.JSONObject;

public interface ServiceRequestListener {
    void onResponseObject(JSONObject response);
    void onResponseArray(JSONArray response);
    void onError(String error);
}
